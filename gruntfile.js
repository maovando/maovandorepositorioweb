module.exports = function(grunt) {
	require('time-grunt')(grunt);
	require('jit-grunt')(grunt, {
		useminPrepare: 'grunt-usemin'
	});
	
	grunt.initConfig({
	  	sass: {
	    	dist: {
	      		files: [{
	      			expand: true,
	          		cwd: 'scss',
	          		src: ['*.scss'],
	          		dest: 'css',
	          		ext: '.css'
	        	}]
	    	}
	  	},

	  	watch: {
	  		files: ['scss/*.scss'],
	  		tasks: ['css']
	  	},

	  	browserSync: {
	  		dev: {
	  			bsFiles: { //browser files
	  				src: [
	  					'css/*.css',
	  					'*.html',
	  					'js/*.js'
	  				]
	  			},
	  			options: {
	  				watchTask: true,
	  				server: {
	  					baseDir: './' //Directorio base para nuestro servidor
	  				}
	  			}
	  		}
	  	},

	  	imagemin: {
	  		dynamic: {
	  			files: [{
	  				expand: true,
	  				cwd: './',
	  				src: 'images/*.{png,gif,jpg,jpeg}',
	  				dest: 'dist/'
	  			}]
	  		}
	  	},
    });

	// Load up tasks

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-imagemin');


	// Default task

	grunt.registerTask('css', ['sass']);
	grunt.registerTask('default', ['browserSync', 'watch']);
	grunt.registerTask('img:compress', ['imagemin']);	
};