const   gulp = require('gulp'),
        sass = require('gulp-sass'),
        browserSync = require('browser-sync'),
        pug = require('gulp-pug');

gulp.task('sass', function() {
    return gulp.src('./scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.reload({ stream: true })) // 
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: './'  //
        }
    });
});


gulp.task('html', function() {
    return gulp.src('./*.html')
    .pipe(browserSync.reload({ stream: true }))
});

gulp.task('watch', function() {
    gulp.watch('./scss/*.scss', gulp.parallel('sass'));
    gulp.watch('./*.html', gulp.parallel('html'));
});

gulp.task('clean', function(){
	return del(['distri']);
});

gulp.task('copyfonts', function(){
	gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
	.pipe(gulp.dest('./distri/fonts'));
});

gulp.task('imagemin', function (){
	// body...
	return gulp.src('./images/*')
	.pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
	.pipe(gulp.dest('distri/images'));
});

gulp.task('usemin', function(){
	return gulp.src('./*.html')
		.pipe(flatmap(function(stream, file){
			return stream
				.pipe(usemin({
					css: [rev()],
					html: [function() { return htmlmin({collapsewhitespace: true})}],
					js: [uglify(), rev()],
					inlinejs: [uglify()],
					inlinecss: [cleanCss(), 'concat']
				}))
		}))
});

gulp.task('default', ['clean'], function(){
	gulp.start('copyfonts', 'imagemin', 'usemin');
});

//gulp.task('default', gulp.parallel('html', 'sass', 'browser-sync', 'watch'));